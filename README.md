# L3 TER - FOURMIS

Rapport de projet (en rédaction) :
https://www.overleaf.com/project/5e1c4b859592ed0001755425

Trello :
https://trello.com/b/63jctnkX/in601-fourmis


### HOW TO:

Pour démarrer le programme, ouvrir un terminal dans le dossier où se trouve ce fichier.  
Se déplacer dans le dossier 'SRC', puis exécuter le main avec la version 3 de python :  
```
cd SRC
python main.py
```  

Une fenêtre de paramètres s'ouvre, contenant plusieurs paramètres modifiables au besoin :  
- Hauteur (en nombre de cellules);
- Largeur (en nombre de cellules);
- Taille cellules (en pixels);
- Tick speed (en ms), également modifiable pendant la simulation;
- Nombre de fourmis au départ;
- Taille maximale des ressources;
- Durée d'une phéromone;
- Historique, à cocher  pour pouvoir revenir en arrière pendant la simulation.

Cliquer sur le bouton `Démarrer` pour passer à la prochaine étape. 
Le programme entre alors en phase d'initialisation. 
Durant cette phase, il est possible, à l'aide d'une interface à droite de la grille de simulation, de :

- Placer de nouvelles fourmilières, dont on spécifie le nombre de fourmis;
- Placer de nouvelles ressources, dont on spécifie la taille (inferieure à la taille maximale spécifiée dans la phase précédente);
- Placer des obstacles;
- Supprimer des entités créées.

Pour sortir de cette phase et commencer la simulation, il suffit d'appuyer sur la touche `Entrée`.
Durant la simulation, plusieurs options sont possibles :

- Appuyer sur `Espace` permet de mettre en pause ou relancer la simulation;
- Appuyer sur `r` pendant la pause permet de relancer le programme. Ce dernier reviendra alors en phase d'initialisation;
- Appuyer sur `a` affiche les plus courts chemins entre chaque ressource et fourmilière;
- Cliquer sur une case permet d'obtenir des informations détaillées sur le panneau de gauche;
- Si l'option d'historique a été sélectionnée au lancement du programme, il est possible de naviguer entre chaque tick de la simulation à l'aide des raccourcis `Ctrl+Droite` pour reculer, et `Ctrl+Gauche` pour avancer d'un tick.

