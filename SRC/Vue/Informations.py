from tkinter import *


class Informations(Frame):
	"""Classe Information, qui s'affichera à coté de la grille
	"""
	def __init__(self, parent):
		Frame.__init__(self, parent)

		self.infosGenerales = Text(self, height = 15, width=50)#height en nombre de lignes du widget, pas en pixels
		self.infosCases = Text(self, height = 15, width=50)

		#scrollbar pour les informations relatives à une case
		scrollb = Scrollbar(self, command=self.infosCases.yview)
		self.infosCases['yscrollcommand'] = scrollb.set

		self.infosGenerales.pack()
		self.infosCases.pack()
		self.grid(row=0, column=0, sticky=N+S) # à gauche de la Grille
		self.derniereCaseSurvolee = None



	def updateInfosGenerales(self, nbrTicks, historique, active, init, graine, fourmillieres, fourmis, pheromones, ressources, caseSurvol = None):
		"Met à jour le widget Informations, en affichant les informations importantes générales de la grille ainsi que la case survolée"

		self.infosGenerales.config(state=NORMAL) #Autorise les modifications
		self.clear(self.infosGenerales) #Nettoie

		self.infosGenerales.insert(INSERT, f"Graine : {graine} \n\n")
		self.infosGenerales.insert(INSERT, f"Nombre de fourmillières : {len(fourmillieres)} \n")
		self.infosGenerales.insert(INSERT, f"Nombre de fourmis : {self.sommeDicoListe(fourmis)} \n")
		self.infosGenerales.insert(INSERT, f"Nombre de phéromones présentes : {self.sommeDicoListe(pheromones)} \n")
		self.infosGenerales.insert(INSERT, f"Nombre de ressources restantes :  {sum(ressources.values())} \n")
		self.infosGenerales.insert(INSERT, f"\nTicks : {nbrTicks}\n")

		if caseSurvol is not None:
			self.derniereCaseSurvolee = caseSurvol
			self.infosGenerales.insert(INSERT, f"\nCase survolée : {caseSurvol.x}, {caseSurvol.y}")
		else:
			if self.derniereCaseSurvolee is not None:
				self.infosGenerales.insert(INSERT, f"\nCase survolée : {self.derniereCaseSurvolee.x}, {self.derniereCaseSurvolee.y}")

		if historique:
			self.infosGenerales.insert(INSERT, f"\nHistorique en cours, ticks : {historique}\n")

		self.infosGenerales.config(state=DISABLED)#Empeche les modifications (pour que le user ne puisse pas écrire)

		if not active: #bool vaut interface.active
			if init: #bool valant interface.init
				self.initPhase()
			else:
				self.pause()



	def updateInfosCases(self, coord, estObstacle, fourmis, fourmiliere, pheromones, ressource):
		"Affiche les informations concernant une case spécifique"
		self.infosCases.config(state=NORMAL) #Autorise les modifications
		self.clear(self.infosCases)

		self.infosCases.insert(INSERT, f"Case sélectionnée : {coord.x}, {coord.y}\n")
		if ressource:
			self.infosCases.insert(INSERT, f"\nTaille de la resource : {ressource}\n")
		if pheromones:
			self.infosCases.insert(INSERT, f"\nNombre de phéromones : {len(pheromones)}\n")
			#tableau de couple {typeP : quantité}
			tabPheromones = {}
			for pheromone in pheromones:
				if pheromone.typeP in tabPheromones :
					tabPheromones[pheromone.typeP] += 1
				else :
					tabPheromones[pheromone.typeP] = 1

			for key, value in tabPheromones.items():
				self.infosCases.insert(INSERT, f" {key.name} : {str(value)}\n")

		if fourmiliere:
			self.infosCases.insert(INSERT, f"\nFourmilière : {fourmiliere}\n")

		if fourmis:
			#tableau de couple {depose : quantité}
			tabFourmis = {}
			self.infosCases.insert(INSERT, f"\nNombre de fourmis : {len(fourmis)}\n")
			for fourmi in fourmis:
				if fourmi.depose in tabFourmis:
					tabFourmis[fourmi.depose] += 1
				else:
					tabFourmis[fourmi.depose] = 1

			for key, value in tabFourmis.items():
				self.infosCases.insert(INSERT, f" depose {key.name} : {str(value)}\n")

		if estObstacle:
			self.infosCases.insert(INSERT, f"\nEst un obstacle\n")

		self.infosCases.config(state=DISABLED)#Empeche les modifications (pour que le user ne puisse pas écrire)


	def clear(self, widget):
		"Supprime les éléments du widget"
		widget.delete("1.0", END)


	def sommeDicoListe(self, dico):
		"Fais la somme de la taille des listes du dictionnaire"
		return sum([len(L) for L in dico.values()])


	def pause(self):
		"Affiche que la simulation est en pause"
		self.infosGenerales.config(state=NORMAL)
		self.infosGenerales.insert(INSERT, "\nSimulation en pause...")

		self.infosGenerales.config(state=DISABLED)

	def initPhase(self):
		"Affiche que la simulation est dans sa phase d'initialisation"
		self.infosGenerales.config(state=NORMAL)
		self.infosGenerales.insert(INSERT, "\n\nSimulation en phase d'initialisation :\n- Cliquer sur une case afin d'y insérer l'élément sélectionné dans le menu de droite.")

		self.infosGenerales.config(state=DISABLED)
