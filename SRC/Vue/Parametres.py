from tkinter import Frame, Label, Entry, BooleanVar, Checkbutton, Button


def estNombrePos(entree):
    "Prédicat vérifiant que le paramètre représente un nombre > 0"
    return entree.isdigit() and int(entree) > 0


class Parametres(Frame):
    """
    Fenêtre initiale des options de la grille et de la vitesse de tick
    """

    def __init__(self, parent, hauteurDefaut, largeurDefaut, tailleCellDefaut, tickSpeedDefaut, nbFourmisDefaut, tailleMaxRessDefaut, dureePheroDefaut, fctSimInit):
        Frame.__init__(self, parent)

        self.titre = Label(self, text = "Paramètres de démarrage : ", padx = 20, pady = 10)

        # hauteur
        self.labelHaut = Label(self, text = "Hauteur (en cellules) :")
        self.entreeHaut = Entry(self,
                                validate = "key",
                                validatecommand = (self.register(estNombrePos), "%P"))
        # largeur
        self.labelLarg = Label(self, text = "Largeur (en cellules) :")
        self.entreeLarg = Entry(self,
                                validate = "key",
                                validatecommand = (self.register(estNombrePos), "%P"))
        # taille cellule
        self.labelTailleCell = Label(self, text = "Taille cellules (en px) :")
        self.entreeTailleCell = Entry(self,
                                      validate = "key",
                                      validatecommand = (self.register(estNombrePos), "%P"))
        # tick speed
        self.labelTickSpeed = Label(self, text = "Tick speed (en ms) :")
        self.entreeTickSpeed = Entry(self,
                                     validate = "key",
                                     validatecommand = (self.register(estNombrePos), "%P"))
        # nombre de Fourmis
        self.labelNbFourmis = Label(self, text = "Nombre de fourmis :")
        self.entreeNbFourmis = Entry(self,
                                     validate = "key",
                                     validatecommand = (self.register(estNombrePos), "%P"))
        # taille max de la ressource
        self.labelTailleRess = Label(self, text = "Taille max d'une ressource :")
        self.entreeTailleRess = Entry(self,
                                    validate = "key",
                                    validatecommand = (self.register(estNombrePos), "%P"))
        # graine du générateur aléatoire
        self.labelGraine = Label(self, text = "Graine (vide pour aléatoire) :")
        self.entreeGraine = Entry(self)
        # durée max d'une phéromone (en nb de ticks)
        self.labelDureePhero = Label(self, text = "Durée d'une phéromone\n(en nb. de ticks) :")
        self.entreeDureePhero = Entry(self,
                                    validate = "key",
                                    validatecommand = (self.register(estNombrePos), "%P"))

        #Historique actif ou non
        self.checkHistorique = BooleanVar() #valeur de retour
        self.checkHistorique.set(False)
        self.checkButtonHistorique = Checkbutton(self, var = self.checkHistorique, text = "Historique")

        self.boutonDebut = Button(self,
                                  text = "Démarrer",
                                  command = fctSimInit)

        # valeurs par défaut
        self.entreeHaut.insert(0, str(hauteurDefaut))
        self.entreeLarg.insert(0, str(largeurDefaut))
        self.entreeTailleCell.insert(0, str(tailleCellDefaut))
        self.entreeTickSpeed.insert(0, str(tickSpeedDefaut))
        self.entreeNbFourmis.insert(0, str(nbFourmisDefaut))
        self.entreeTailleRess.insert(0, str(tailleMaxRessDefaut))
        self.entreeGraine.insert(0, "fourmis")
        self.entreeDureePhero.insert(0, str(dureePheroDefaut))

        # packing
        self.titre.pack()
        self.labelHaut.pack()
        self.entreeHaut.pack()
        self.labelLarg.pack()
        self.entreeLarg.pack()
        self.labelTailleCell.pack()
        self.entreeTailleCell.pack()
        self.labelTickSpeed.pack()
        self.entreeTickSpeed.pack()
        self.labelNbFourmis.pack()
        self.entreeNbFourmis.pack()
        self.labelTailleRess.pack()
        self.entreeTailleRess.pack()
        self.labelGraine.pack()
        self.entreeGraine.pack()
        self.labelDureePhero.pack()
        self.entreeDureePhero.pack()
        self.checkButtonHistorique.pack()
        self.boutonDebut.pack()
        self.pack()
