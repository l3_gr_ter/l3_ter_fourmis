from tkinter import *
from tkinter import ttk

from Vue.Aide import Aide

def estNombrePos(entree):
    "Prédicat vérifiant que le paramètre représente un nombre > 0"
    return entree.isdigit() and int(entree) > 0


class ParametresSimu(Frame):
    """
    Fenêtre des options possibles durant la simulation, située à droite de la grille
    """

    def __init__(self, parent, tailleMaxRess):
        Frame.__init__(self, parent)
        self.grid(column=2, sticky="ns")
        self.grid_rowconfigure(2, weight=1)

        # frame affichant les commandes disponibles
        self.fenetreAide = Aide(self)
        self.fenetreAide.grid(row=3, sticky="sew")

        # éditeur d'entités
        self.tailleMaxRess = tailleMaxRess
        self.interfaceEntite()


    def interfaceEntite(self):
        "Interface permettant la création ou suppression d'entités pendant l'initialisation"
        self.creationEntite = Frame(self)
        self.valeurEntite = IntVar() #variable servant a savoir ce qui va être créé
        self.valeurEntite.set(1)

        #Remplacement par une fourmilière
        self.creationEntite.choixFourmiliere = Radiobutton(
            self.creationEntite,
            variable = self.valeurEntite,
            value = 1,
            text = "Placer une fourmilière, avec un nombre de fourmis :")
        self.creationEntite.entreeNbFourmis = Entry(
            self.creationEntite,
            validate = "key",
            validatecommand = (self.register(estNombrePos), "%P"))
        self.creationEntite.entreeNbFourmis.insert(0, str(200))

        #Remplacement par une ressource
        self.creationEntite.choixRess = Radiobutton(
            self.creationEntite,
            variable = self.valeurEntite,
            value = 2,
            text = "Placer une ressource, d'une taille :")
        self.creationEntite.entreeTailleRess = Entry(
            self.creationEntite,
            validate = "key",
            validatecommand = (self.register(self.estTailleRessValide,), "%P"))
        self.creationEntite.entreeTailleRess.insert(0, str(self.tailleMaxRess))

        #Remplacement par un obstacle
        self.creationEntite.choixObs = Radiobutton(
            self.creationEntite,
            variable = self.valeurEntite,
            value = 3,
            text = "Placer un obstacle")

        #Suppression d'entité
        self.creationEntite.choixSuppr = Radiobutton(
            self.creationEntite,
            variable = self.valeurEntite,
            value = 0,
            text = "Supprimer des entités")

        #Affichage
        self.creationEntite.choixFourmiliere.pack()
        self.creationEntite.entreeNbFourmis.pack()
        self.creationEntite.choixRess.pack()
        self.creationEntite.entreeTailleRess.pack()
        self.creationEntite.choixObs.pack()
        self.creationEntite.choixSuppr.pack()
        self.creationEntite.grid(row=0, pady=20, sticky="new")


    def estTailleRessValide(self, entree):
        "Prédicat vérifiant que la taille de ressource est bien un nombre entre 0 et la taille maximale d'une ressource"
        return entree.isdigit() and self.tailleMaxRess >= int(entree) > 0


    def demarrage(self, tickSpeed):
        "Supprime la fenêtre d'édition et affiche le modificateur de tickspeed et le sélecteur de phéromone"
        # suppression de la frame d'édition
        self.creationEntite.destroy()

        # modificateur de tickspeed
        self.tickVar = Scale(
            self,
            orient='horizontal',
            from_=1,
            to=1000,
            resolution=1,
            tickinterval=0,
            length=300,
            label='Vitesse du tick (1 à 1000 ms)')
        self.tickVar.set(tickSpeed)
        self.tickVar.grid(row=0, pady=10, sticky="new")

        self.sep = ttk.Separator(self, orient="horizontal")
        self.sep.grid(row=1, sticky="ew")


    def reset(self):
        self.tickVar.destroy()
        self.sep.destroy()
        self.interfaceEntite()
