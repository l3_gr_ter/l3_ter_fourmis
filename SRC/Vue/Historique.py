from collections import deque
from pickle import dumps, loads

from Outils.Coordonnees import Coordonnees
from Controleur.Oracle import Oracle


class Historique:
	"""
	La classe Historique conserve l'historique des oracles passés pendant n positions
	Elle se compose de 2 parties:
		- une fifo contenant les n dernières positions jusqu'à la position courante
		- un moyen de calculer les n positions précédentes (répétable), dès que l'on revient trop en arrière
	Lorsque la simulation redémarre l'hstorique supprime les données qui ne sont pas dans la fifo
	"""

	def __init__(self, oracleInit, taille=10):
		# partie concernant la fifo
		self.fileHistorique = deque() # fifo de taille tailleHistorique
		self.tailleHistorique = taille # taille de la fifo

		# partie concernant les calculs d'anciens historiques
		self.oracleInit = oracleInit
		self.tabAnciensHistoriques = []
		self.nbrDecalage = 1 # nombre de retours en arrière demandés pour compléter l'historique


	def tick(self, oracle):
		"A chaque tick, supprime l'historique le plus ancien (si nécessaire) et ajoute le plus récent"
		# sérialisation de l'oracle avec le module pickle
		self.fileHistorique.appendleft(dumps(oracle))

		if len(self.fileHistorique) > self.tailleHistorique: # historique trop grand
			self.fileHistorique.pop()

		# on supprime tout ce qui concerne les anciens historiques
		self.nbrDecalage = 1
		self.tabAnciensHistoriques = []


	def reculeTick(self, nbrTicks, tickDifference):
		"Affiche l'historique de la position précédente"

		# le tick est dans l'historique de la fifo
		if tickDifference <= self.tailleHistorique:
			oracle = self.fileHistorique[tickDifference - 1]

		# l'historique est encore avant, mais il est déjà calculé
		elif tickDifference <= self.tailleHistorique * self.nbrDecalage:
			oracle = self.tabAnciensHistoriques[tickDifference - 1 - self.tailleHistorique]

		# l'historique n'a pas encore été calculé
		else:
			self.nbrDecalage += 1

			# initialisation du nouvel oracle, permettant de refaire les calculs
			newOracle = loads(self.oracleInit)

			# déplacement au premier tick dont l'oracle sera ajouté à l'historique
			premierTick = max(0, nbrTicks - 1 - self.tailleHistorique * self.nbrDecalage)
			newOracle.avanceTicks(premierTick)

			# tableau contenant temporairement les nouveaux oracles calculés
			newTab = [dumps(newOracle)]

			# on construit les n historiques précédents
			for _ in range(premierTick, nbrTicks - 1 - tickDifference):
				newOracle.tick()
				newTab.append(dumps(newOracle))

			# ajout dans le sens chronologique inverse
			self.tabAnciensHistoriques.extend(reversed(newTab))

			oracle = self.tabAnciensHistoriques[tickDifference - 1 - self.tailleHistorique]

		return loads(oracle) # désérialisation


	def avanceTick(self, tickDifference):
		"Affiche la position suivante, mais toujours dans l'historique"
		if tickDifference <= self.tailleHistorique: # le tick est dans l'historique de la fifo
			return loads(self.fileHistorique[tickDifference - 1])
		else:
			return loads(self.tabAnciensHistoriques[tickDifference - 1 - self.tailleHistorique])








