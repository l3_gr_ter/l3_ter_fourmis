from tkinter import Frame, BooleanVar, Tk, Toplevel
from random import randrange
from sys import maxsize
from pickle import dumps, loads

from Outils.Coordonnees import Coordonnees
from Outils.JumpPointSearch import JPSBidir
from Controleur.Oracle import Oracle
from Vue.Historique import Historique
from Vue.Grille import Grille
from Vue.Parametres import Parametres
from Vue.Informations import Informations
from Vue.ParametresSimu import ParametresSimu
from Modele.Pheromone import Pheromone

class Interface(Frame):
    """
    Fais le lien entre la simulation et l'utilisateur par une interface graphique
    """

    def __init__(self, racine):
        Frame.__init__(self, racine)

        # flag indiquant si la simulation est en cours
        self.active = False
        # flag indiquant si l'historique est calculé à chaque tick
        self.activeHistorique = False
        # flag indiquant si la fenetre d'aide existe déjà
        self.activeAide = False

        # intervalle entre chaque tick
        self.tickSpeed = 100 # valeur par défaut

        # nombre de ticks depuis le début de la simulation
        self.nbrTicks = 0

        # nombre de ticks (actifs) depuis le début de la simulation, utile pour afficher l'historique
        self.nbrTicksActifs = 0

        # fenêtre de paramètres
        self.fenetreParam = Parametres(parent = self,
                                       hauteurDefaut = 25,
                                       largeurDefaut = 25,
                                       tailleCellDefaut = 20,
                                       tickSpeedDefaut = self.tickSpeed,
                                       nbFourmisDefaut = 500,
                                       tailleMaxRessDefaut = 100,
                                       dureePheroDefaut = 15,
                                       fctSimInit = self.simInit)

        #coordonnée de la position ou l'utilisateur a clické
        self.coordonneeClick = None

        self.pack()


    def simInit(self):
        """
        Initialisation de la simulation
        Fonction appelée par le bouton "Simuler" du cadre "paramètres"
        Supprime le cadre et crée la grille
        """
        self.init = True #Signifie qu'on est en phase d'initialisation

        # récupération des paramètres
        self.graine = self.fenetreParam.entreeGraine.get()
        if not self.graine: # graine vide, remplacée par une graine aléatoire
            self.graine = randrange(maxsize)
        self.oracle = Oracle(self.graine)
        self.grille = Grille(self,
                             int(self.fenetreParam.entreeHaut.get()),
                             int(self.fenetreParam.entreeLarg.get()),
                             int(self.fenetreParam.entreeTailleCell.get()))
        self.tickSpeed = int(self.fenetreParam.entreeTickSpeed.get())
        self.activeHistorique = self.fenetreParam.checkHistorique.get()

        #initialisation des coordonnées de la Fourmilière et de la Ressource par défaut,
        #ainsi que du nombre de fourmis au départ, de la taille max d'une ressource et de la durée d'une phéromone
        self.coordFourmiliereDepart = Coordonnees(
            round(int(self.fenetreParam.entreeHaut.get())/2),
            round(int(self.fenetreParam.entreeLarg.get())/4)
            )
        self.coordRessDepart = Coordonnees(
            round(int(self.fenetreParam.entreeHaut.get())/2),
            round(int(self.fenetreParam.entreeLarg.get())/4 + int(self.fenetreParam.entreeHaut.get())/2)
            )
        self.nbFourmisDepart = int(self.fenetreParam.entreeNbFourmis.get())
        self.tailleRessMax = int(self.fenetreParam.entreeTailleRess.get())
        Pheromone.duree = int(self.fenetreParam.entreeDureePhero.get())

        # suppression de la fenêtre de paramètres et création des panneaux gauche et droite
        self.fenetreParam.destroy()
        self.fenetreSimulation = ParametresSimu(self, self.tailleRessMax)
        self.informations = Informations(self) #Initialisation de la frame Information

        # liaison des touches
        self.grille.bind_all("<Return>", self.demarrage) #Appelle la méthode de démarrage à l'appui sur entrée pendant la phase d'initailisation
        self.grille.bind_all("<Key>", self.key) # Appelle la méthode key à chaque fois qu'on appuie sur une touche
        self.grille.bind("<Button-1>", self.click) # Appelle la méthode click à chaque clic sur la simu
        self.grille.bind_all("<Control-Right>", self.avanceTick) # A chaque click sur Ctrl-Droite
        self.grille.bind_all("<Control-Left>", self.reculeTick) # A chaque click sur Ctrl-Left
        self.grille.bind("<Motion>", self.hover) # Au survol de la souris

        self.grille.grid(row=0, column=1) # à droite d'Informations

        # les coordonnées accessibles par les fourmis sont entre (0, 0) et coordMax compris
        coordMax = Coordonnees(self.grille.hauteur // self.grille.tailleCell, self.grille.largeur // self.grille.tailleCell)
        nord = [Coordonnees(i, -1) for i in range(-1, coordMax.x + 1)]
        est = [Coordonnees(coordMax.x, i) for i in range(-1, coordMax.y + 1)]
        sud = [Coordonnees(i, coordMax.x) for i in range(-1, coordMax.y + 1)]
        ouest = [Coordonnees(-1, i) for i in range(-1, coordMax.y + 1)]
        Oracle.obstaclesDefaut.update(nord, est, sud, ouest)

        #Met en place dans l'oracle l'unique fourmilière, l'unique ressource, le tableau obstacles de bordure et les fourmis dont le nombre a été précisé au lancement du programme
        self.oracle.setup(coordFrmliere = self.coordFourmiliereDepart,
                          coordRess = self.coordRessDepart,
                          nbFourmis = self.nbFourmisDepart,
                          tailleMaxRess = self.tailleRessMax)
        self.informations.updateInfosGenerales(self.nbrTicks, False, self.active, self.init, self.oracle.graine, self.oracle.fourmilieres, self.oracle.fourmis, self.oracle.pheromones, self.oracle.ressources)

        # setup la grille avec les fourmilières, obstacles et ressources initiales
        self.grille.setupGrid(self.oracle.fourmilieres, self.oracle.obstacles, self.oracle.ressources)


    def tick(self):
        "Exécute le tick de l'oracle et dessine les entités"
        if not self.active:
            return self.tickSpeed

        elif self.nbrTicks == 0:
            self.grille.tick(self.oracle.fourmis, self.oracle.fourmilieres, self.oracle.pheromones, self.oracle.ressources, self.oracle.obstacles, self.oracle.tailleMaxRess)
            self.nbrTicks += 1
            return self.tickSpeed

        # compteurs de ticks incrémentés
        self.nbrTicks +=1
        self.nbrTicksActifs = self.nbrTicks

        # mise à jour de l'Historique
        if(self.activeHistorique):
            self.historique.tick(self.oracle)

        # exécution du tick de l'oracle
        self.oracle.tick()

        # mise à jour de la grille
        self.grille.tick(self.oracle.fourmis, self.oracle.fourmilieres, self.oracle.pheromones, self.oracle.ressources, self.oracle.obstacles, self.oracle.tailleMaxRess)

        # mise a jour des informations+
        self.informations.updateInfosGenerales(self.nbrTicks, False, self.active, self.init, self.oracle.graine, self.oracle.fourmilieres, self.oracle.fourmis, self.oracle.pheromones, self.oracle.ressources)

        # mise a jour des informations pour la case sélectionnée
        if self.coordonneeClick is not None:
            self.informations.updateInfosCases(self.coordonneeClick,
                                               self.coordonneeClick in self.oracle.obstacles,
                                               self.oracle.fourmis.get(self.coordonneeClick, []),
                                               self.oracle.fourmilieres.get(self.coordonneeClick, None),
                                               self.oracle.pheromones.get(self.coordonneeClick, []),
                                               self.oracle.ressources.get(self.coordonneeClick, 0))

        # modification de la vitesse du tick via le potentiomètre de fenetreSimulation
        self.tickSpeed = self.fenetreSimulation.tickVar.get()

        return self.tickSpeed


    def demarrage(self, event):
        "Fonction de callback, sort la simulation de sa phase d'initialisation à l'appui sur entrée, ne marche plus pendant la simulation"
        if(self.init == True):
            self.oracleInit = dumps(self.oracle) # copie de l'état initial
            if self.activeHistorique:
                self.historique = Historique(self.oracleInit, 10)

            # mise en mémoire de tous les chemins Fourmilière - Ressource
            for coordF in self.oracle.fourmilieres:
                for coordR in self.oracle.ressources:
                    chemin = JPSBidir(coordF, coordR, self.oracle.obstacles)
                    self.grille.dessinChemin(chemin)

            self.informations.updateInfosGenerales(self.nbrTicks, False, self.active, self.init, self.oracle.graine, self.oracle.fourmilieres, self.oracle.fourmis, self.oracle.pheromones, self.oracle.ressources)

            # màj panneau droit
            self.fenetreSimulation.demarrage(self.tickSpeed)

            self.init = False
            self.active = True


    def key(self, event):
        "Fonction de callback, modifie la simulation en fonction de la key envoyée par l'utilisateur et de l'état de la simulation"
        if(event.char == ""): # Caractère spécial
            pass

        # Pause/reprise avec espace, seulement hors de l'initialisation
        elif self.init is False and event.char == ' ':
            if self.active:
                self.informations.pause() # affiche une pause
                self.active = False
            else:
                self.active = True

        # affichage des chemins Fourmiliere - Ressource, seulement hors de l'initialisation
        elif self.init is False and event.char == 'a':
            self.grille.toggleHighlights()

        # reset de la simulation à l'état d'initialisation, seulement si elle est en pause, faisable pendant l'initialisation
        elif self.init is False and self.active is False and event.char == 'r':
            self.oracle = loads(self.oracleInit) # récupération de l'état initial
            self.nbrTicks = 0
            self.grille.setupGrid(self.oracle.fourmilieres,self.oracle.obstacles, self.oracle.ressources)
            self.init = True
            self.fenetreSimulation.reset()
            self.informations.initPhase() #affiche l'initialisation
            self.informations.updateInfosGenerales(self.nbrTicks, False, self.active, self.init, self.oracle.graine, self.oracle.fourmilieres, self.oracle.fourmis, self.oracle.pheromones, self.oracle.ressources)


    def avanceTick(self, event):
        "Avance la simulation d'un tick si elle est en pause, ni en cours d'initialisation"
        if(self.active == True or self.init == True):
            return

        self.nbrTicksActifs += 1

        if self.nbrTicks + 1 == self.nbrTicksActifs: # les ticks sont synchronisés
            #Updates
            self.nbrTicks += 1
            if(self.activeHistorique): #Historique actif
                self.historique.tick(self.oracle)
            self.oracle.tick()

            #Affichages
            self.grille.tick(self.oracle.fourmis, self.oracle.fourmilieres, self.oracle.pheromones, self.oracle.ressources, self.oracle.obstacles, self.oracle.tailleMaxRess)
            self.informations.updateInfosGenerales(self.nbrTicks, False, self.active, self.init, self.oracle.graine, self.oracle.fourmilieres, self.oracle.fourmis, self.oracle.pheromones, self.oracle.ressources)

        if self.nbrTicks == self.nbrTicksActifs: # retour au tick initialement mis en pause de la simulation
            #Affichages
            self.grille.tick(self.oracle.fourmis, self.oracle.fourmilieres, self.oracle.pheromones, self.oracle.ressources, self.oracle.obstacles, self.oracle.tailleMaxRess)
            self.informations.updateInfosGenerales(self.nbrTicks, False, self.active, self.init, self.oracle.graine, self.oracle.fourmilieres, self.oracle.fourmis, self.oracle.pheromones, self.oracle.ressources)

        else: #Avancement dans l'historique
            #Updates
            tempOracle = self.historique.avanceTick(self.nbrTicks - self.nbrTicksActifs)
            #Affichages
            self.grille.tick(tempOracle.fourmis, tempOracle.fourmilieres, tempOracle.pheromones, tempOracle.ressources, tempOracle.obstacles, tempOracle.tailleMaxRess)
            self.informations.updateInfosGenerales(self.nbrTicks, self.nbrTicksActifs, self.active, self.init, tempOracle.graine, tempOracle.fourmilieres, tempOracle.fourmis, tempOracle.pheromones, tempOracle.ressources)


    def reculeTick(self, event):
        "Affiche l'historique au tick précédent seulement si la simulation n'est ni en pause, ni en cours d'initialisation"
        if(self.active == True or self.nbrTicksActifs == 1 or self.activeHistorique == False or self.init == True):
            return
        self.nbrTicksActifs -= 1
        tempOracle = self.historique.reculeTick(self.nbrTicks, self.nbrTicks - self.nbrTicksActifs)
        #Affichages
        self.grille.tick(tempOracle.fourmis, tempOracle.fourmilieres, tempOracle.pheromones, tempOracle.ressources, tempOracle.obstacles, tempOracle.tailleMaxRess)
        self.informations.updateInfosGenerales(self.nbrTicks, self.nbrTicksActifs, self.active, self.init, tempOracle.graine, tempOracle.fourmilieres, tempOracle.fourmis, tempOracle.pheromones, tempOracle.ressources)



    def click(self, event):
        "Renvoie la position où l'utilisateur a cliqué"
        self.grille.focus_set()
        self.oracle.fourmilieres
        coord = Coordonnees(event.x // self.grille.tailleCell, event.y // self.grille.tailleCell)
        self.coordonneeClick = coord
        if self.init:
            self.summonEntity(
                coord,
                self.fenetreSimulation.valeurEntite.get(),
                int(self.fenetreSimulation.creationEntite.entreeNbFourmis.get()),
                int(self.fenetreSimulation.creationEntite.entreeTailleRess.get()))
        else:
            pass
        self.informations.updateInfosCases(coord,
                                           coord in self.oracle.obstacles,
                                           self.oracle.fourmis.get(coord, []),
                                           self.oracle.fourmilieres.get(coord, None),
                                           self.oracle.pheromones.get(coord, []),
                                           self.oracle.ressources.get(coord, 0))

    def hover(self, event):
        "Renvoie la position que l'utilisateur survole avec sa souris"
        self.grille.focus_set()
        coord = Coordonnees(event.x // self.grille.tailleCell, event.y // self.grille.tailleCell)
        self.informations.updateInfosGenerales(self.nbrTicks, False, self.active, self.init, self.oracle.graine, self.oracle.fourmilieres, self.oracle.fourmis, self.oracle.pheromones, self.oracle.ressources, coord)


    def summonEntity(self, coord, valEnt, nbF, tailleR):
        "Supprime l'ancienne entité présente sur la case coord s'il y a, et créé une entité aux coordonnées coord, selon la valeur de valEnt : 1 - une fourmilière de nbF fourmis, 2 - une ressource de taille tailleR, 3 - un obstacle"
        if coord in self.oracle.fourmilieres.keys(): # si l'entité était une fourmilière, on la supprime de l'oracle et de la grille
            self.oracle.supprimeFourmiliere(coord)
            self.grille.delete(self.grille.nests.pop(coord))
        elif coord in self.oracle.ressources.keys(): #si l'entité était une ressource, on la supprime de l'oracle et de la grille
            self.oracle.supprimeRessource(coord)
            self.grille.delete(self.grille.ress.pop(coord))
        elif coord in self.oracle.obstacles: #si l'entité était un obstacle, on le supprime de l'oracle et de la grille
            self.oracle.supprimeObstacle(coord)
            self.grille.delete(self.grille.obstacles.pop(coord))
        else:
            pass
        if valEnt == 1:
            idF = self.oracle.ajoutFourmiliere(coord)
            self.oracle.ajoutFourmis(coord, idF, nbF)
            self.grille.dessinFourmiliere(coord)
        elif valEnt == 2:
            self.oracle.ajoutRessource(coord, tailleR)
            self.grille.dessinRessource(coord)
        elif valEnt == 3:
            self.oracle.obstacles.add(coord)
            self.grille.dessinObstacle(coord)

        self.informations.updateInfosGenerales(self.nbrTicks, False, self.active, self.init, self.oracle.graine, self.oracle.fourmilieres, self.oracle.fourmis, self.oracle.pheromones, self.oracle.ressources)
