from tkinter import Frame, Label, ttk

class Aide(Frame):
	"""Frame qui affiche les commandes disponibles"""

	def __init__(self, parent):
		Frame.__init__(self, parent)

		textVar = \
"""		Commandes :
Entrée : Démarre la simulation après avoir mis en place
l'agencement de la grille.
Espace : Met en pause/relance la simulation.
A : affiche les chemins des fourmilières aux ressources
Quand la simulation est en pause :
	- Ctrl+Droite : Avancer d'un tick.
	- Ctrl+Gauche : Reculer d'un tick (Seulement si
	  l'historique est actif).
	- R : réinitialisation de la simulation.
Cliquer sur une case affiche des informations
complémentaires.
"""

		self.sep = ttk.Separator(self, orient="horizontal")
		self.label = Label(self, justify="left", text=textVar)
		self.sep.grid(pady=10, sticky="ew")
		self.label.grid(sticky="s")
