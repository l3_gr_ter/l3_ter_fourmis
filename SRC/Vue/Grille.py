from tkinter import Canvas
import cmath, math

from Outils.Coordonnees import Coordonnees
from Outils.Direction import Direction
from Modele.Pheromone import Pheromone
from Modele.TypePheromone import TypePheromone

class Grille(Canvas):
    """
        La classe Grille gère l'affichage de la Grille et des
        fourmis à l'intérieur. Elle possède les attributs :
        - hauteur, la hauteur de la grille
        - largeur, la largeur de la grille
        - tailleCell, la taille d'une Cellule
        - objets, contenant les coordonnées des entités de la grille

    """
    def __init__(self, Junko, haut, larg, taille, color = "white"): #Junko est le widget qui contient le canvas (ici la frame, spécialisée par Interface)
        if haut <= 0 or larg <= 0 or taille <= 0:
            return
        Canvas.__init__(self, Junko, height = haut*taille, width = larg*taille, bg = color)
        self.hauteur = haut * taille # à titre informatif
        self.largeur = larg * taille # à titre informatif
        self.tailleCell = taille
        self.fourmis = {} # tous les objets sauf les fourmilières
        self.nests = {} # les fourmilières
        self.pheros = {} # les phéromones
        self.ress = {} # les ressources
        self.obstacles = {} #les obstacles
        self.highlights = {} #le surlignage des cases

        self.showHighlights = False # flag déterminant si les highlights sont affichés
        self.showPhero = "show" # détermine quelles phéromones sont affichées


    def setupGrid(self, fourmilieres, obstacles, ressources):
        "Remet l'état de la grille à un etat initial ou ne sont présents que les obstacles, fourmilières et ressources passées en paramètre"
        for i in self.fourmis: # on dégage les fourmis
            self.delete(self.fourmis[i])
        for i in self.pheros: # et les phéromones
            self.delete(self.pheros[i])
        for i in self.nests: # ainsi que les fourmilières
            self.delete(self.nests[i])
        for i in self.ress: # puis les ressources
            self.delete(self.ress[i])
        for i in self.obstacles: # et enfin les obstacles
            self.delete(self.obstacles[i])

        # puis on remet tout à zéro
        self.fourmis = {}
        self.pheros = {}
        self.nests = {}
        self.obstacles = {}
        self.ress = {}

        #on trace les lignes délimitant la grille
        for i in range(self.tailleCell, self.largeur, self.tailleCell):
            self.create_line(i, 0, i, self.hauteur)
        for i in range(self.tailleCell, self.hauteur, self.tailleCell):
            self.create_line(0, i, self.largeur, i)

        for Ibuki in fourmilieres: #fourmilieres contient des coordonnées de fourmilières, qu'on dessine
            self.dessinFourmiliere(Ibuki)

        for Hajime in obstacles: #obstacles contient des coordonnées d'obstacles, qu'on dessine aussi
            self.dessinObstacle(Hajime)

        for Byakuya in ressources: #ressources contient des coordonnées de ressources, qu'on dessine également
            self.dessinRessource(Byakuya)



    def dessinFourmiliere(self, Kyoko): #Kyoko est une coordonnée de fourmilière
        "Dessine une fourmilière aux coordonnées Kyoko, puis ajoute l'objet créé à la grille."
        x1 = Kyoko.x * self.tailleCell + round(self.tailleCell * 0.15)
        x2 = Kyoko.x * self.tailleCell + round(self.tailleCell * 0.85)
        x3 = Kyoko.x * self.tailleCell + round(self.tailleCell * 0.40)
        x4 = Kyoko.x * self.tailleCell + round(self.tailleCell * 0.60)
        y1 = Kyoko.y * self.tailleCell + round(self.tailleCell * 0.15)
        y2 = Kyoko.y * self.tailleCell + round(self.tailleCell * 0.85)
        y3 = Kyoko.y * self.tailleCell + round(self.tailleCell * 0.45)
        o_id = self.create_polygon(x1, y1,
                                    x2, y1,
                                    x2, y2,
                                    x4, y2,
                                    x4, y3,
                                    x3, y3,
                                    x3, y2,
                                    x1, y2,
                                    fill = "black") # ça a la forme d'une entrée !!!
        self.nests[Kyoko] = o_id


    def dessinFourmi(self, Chiaki, Ori): # Chiaki est une coordonnée de fourmi, Ori est l'orientation de la fourmi
        "Dessine une fourmi aux coordonnées Chiaki, orientée selon la direction Ori, puis ajoute l'objet créé à la grille."

        centre = complex(Chiaki.x * self.tailleCell + round(self.tailleCell * 0.5),Chiaki.y * self.tailleCell + round(self.tailleCell * 0.5))

        # Initialisation des coordonnées des points d'une forme de fourmi (un losange)
        x1 = Chiaki.x * self.tailleCell + round(self.tailleCell * 0.5)
        x2 = Chiaki.x * self.tailleCell + round(self.tailleCell * 0.3)
        x3 = Chiaki.x * self.tailleCell + round(self.tailleCell * 0.7)
        y1 = Chiaki.y * self.tailleCell + round(self.tailleCell * 0.2)
        y2 = Chiaki.y * self.tailleCell + round(self.tailleCell * 0.65)
        y3 = Chiaki.y * self.tailleCell + round(self.tailleCell * 0.8)
        coords = [(x1, y1), (x2, y2), (x1, y3), (x3, y2)]

        if(Ori == Direction.NE):
            angle = cmath.exp((math.pi/4)*1j)
            o_id = self.create_polygon(self.rotation(coords, angle, centre), outline = "black", fill = "black", width = 1)
        elif(Ori == Direction.E):
            angle = cmath.exp((math.pi/2)*1j)
            o_id = self.create_polygon(self.rotation(coords, angle, centre), outline = "black", fill = "black", width = 1)
        elif(Ori == Direction.SE):
            angle = cmath.exp(((3*math.pi)/4)*1j)
            o_id = self.create_polygon(self.rotation(coords, angle, centre), outline = "black", fill = "black", width = 1)
        elif(Ori == Direction.S):
            angle = cmath.exp((math.pi)*1j)
            o_id = self.create_polygon(self.rotation(coords, angle, centre), outline = "black", fill = "black", width = 1)
        elif(Ori == Direction.SO):
            angle = cmath.exp(-((3*math.pi)/4)*1j)
            o_id = self.create_polygon(self.rotation(coords, angle, centre), outline = "black", fill = "black", width = 1)
        elif(Ori == Direction.O):
            angle = cmath.exp(-(math.pi/2)*1j)
            o_id = self.create_polygon(self.rotation(coords, angle, centre), outline = "black", fill = "black", width = 1)
        elif(Ori == Direction.NO):
            angle = cmath.exp(-(math.pi/4)*1j)
            o_id = self.create_polygon(self.rotation(coords, angle, centre), outline = "black", fill = "black", width = 1)
        else: # Soit la fourmi regarde vers le nord, soit la direction est indéfinie
            o_id = self.create_polygon(coords, outline = "black", fill = "black", width = 1)

        self.fourmis[Chiaki] = o_id


    def rotation(self, coords, angle, centre):
        "Effectue la rotation dans le sens trigo du point aux coordonnées coords sur un angle exprimé en radians autour du point de coordonnées centre."
        Junko = []
        for x, y in coords:
            v = angle * (complex(x, y) - centre) + centre
            Junko.append((v.real, v.imag))
        return Junko


    def couleurRess(self, taille, tailleMax):
        "Attribue une couleur à une ressource d'une certaine taille par rapport à sa taille maximum"
        chap = hex(255 - round(255 * (taille/tailleMax)))
        if len(chap) < 4:
            chap = "0" + chap[2:]
        else:
            chap = chap[2:]
        return "#" + chap + chap + chap # de noir (pleine) à blanc (vide)


    def couleurPhero(self, duree, typeP):
        "Attribue une couleur à une phéromone selon son type"
        chap = hex(255 - round(255 * (duree/Pheromone.duree)))
        if len(chap) < 4:
            chap = "0" + chap[2:]
        else:
            chap = chap[2:]

        if typeP == TypePheromone.Aller: # gradient de vert
            chap = "#" + chap + "ff" + chap
        elif typeP == TypePheromone.Retour: # gradient de rouge
            chap = "#" + "ff" + chap + chap
        return chap


    def dessinRessource(self, Hiyoko): #Hiyoko est une coordonnée de ressource
        "Dessine une ressource aux coordonnées Hiyoko et ajoute l'objet créé à la grille."
        x1 = Hiyoko.x * self.tailleCell + round(self.tailleCell * 0.5)
        y1 = Hiyoko.y * self.tailleCell + round(self.tailleCell * 0.5)
        x2 = Hiyoko.x * self.tailleCell
        y2 = Hiyoko.y * self.tailleCell
        o_id = self.create_polygon(x1, y2,
                                    x2 + self.tailleCell, y1,
                                    x1, y2 + self.tailleCell,
                                    x2, y1,
                                    fill = "black",
                                    outline = "black",
                                    width = 1) #c'est un losange, passant de noir a blanc à mesure que la ressource se vide
        self.ress[Hiyoko] = o_id


    def dessinPheromone(self, coord, typeP):
        "Dessine une phéromone aux coordonnées coord, colorée différement selon son typeP, puis ajoute l'objet créé à la grille."
        couleur = "red"
        x1 = coord.x * self.tailleCell
        x2 = (coord.x + 1) * self.tailleCell
        y1 = coord.y * self.tailleCell
        y2 = (coord.y + 1) * self.tailleCell

        if typeP == TypePheromone.Retour:
            couleur = "red"

        elif typeP == TypePheromone.Aller:
            couleur = "green"

        o_id = self.create_polygon(x1, y1, x1, y2, x2, y2, x2, y1, fill=couleur, outline="black", tags=couleur)
        self.tag_lower(o_id)
        self.pheros[coord] = o_id

        """
        # affichage
        if self.showPhero != "show" and self.showPhero != couleur:
            self.itemconfigure(o_id, state="hidden")
        """


    def dessinObstacle(self, coord):
        "Dessine un obstacle aux coordonnées coord et ajoute l'id aux obstacles"
        x1 = coord.x * self.tailleCell
        x2 = (coord.x + 1) * self.tailleCell
        y1 = coord.y * self.tailleCell
        y2 = (coord.y + 1) * self.tailleCell
        o_id = self.create_polygon(x1, y1, x1, y2, x2, y2, x2, y1, fill="black")
        self.obstacles[coord] = o_id


    def highlight(self, coord, couleur):
        "Met en évidence une case et ajoute l'id aux highlights"
        if coord in self.highlights:
            self.delete(self.highlights[coord])
        x1 = coord.x * self.tailleCell
        x2 = (coord.x + 1) * self.tailleCell
        y1 = coord.y * self.tailleCell
        y2 = (coord.y + 1) * self.tailleCell
        o_id = self.create_polygon(x1, y1, x1, y2, x2, y2, x2, y1, fill=couleur)
        self.itemconfigure(o_id, state=("normal" if self.showHighlights else "hidden"))
        self.tag_lower(o_id) # la case est mise en avant sans recouvrir les sprites présents
        self.highlights[coord] = o_id


    def dessinChemin(self, coords):
        "Dessine un chemin avec le départ en vert et l'arrivée en bleu"
        self.highlight(coords[0], "blue")
        self.highlight(coords[-1], "green")
        for coord in coords[1:-1]:
            self.highlight(coord, "orange")


    def toggleHighlights(self):
        self.showHighlights = not self.showHighlights
        etat = "normal" if self.showHighlights else "hidden"
        for item in self.highlights.values():
            self.itemconfigure(item, state=etat)


    def tick(self, fourmis,fourmilieres, pheromones, ressources, obstacles, tailleMax):
        "Mise à jour des éléments de la grille"

        popFourmis = self.fourmis.keys() - fourmis.keys()  #les fourmis à virer
        pushFourmis = fourmis.keys() - self.fourmis.keys() #les fourmis à ajouter

        popFourmilieres = self.nests.keys() - fourmilieres.keys() #les fourmilières à virer
        pushFourmilieres = fourmilieres.keys() - self.nests.keys() #les fourmilières à ajouter

        popPheros = self.pheros.keys() - pheromones.keys()  #les phéromones à virer
        pushPheros = pheromones.keys() - self.pheros.keys() #les phéromones à ajouter

        popRessources = self.ress.keys() - ressources.keys()  #les ressources à virer
        pushRessources = ressources.keys() - self.ress.keys() #les ressources à ajouter

        popObstacles = self.obstacles.keys() - obstacles
        pushObstacles = obstacles - self.obstacles.keys()

        for Miu in pushFourmis:
            self.dessinFourmi(Miu, fourmis[Miu][0].direction)
        for Himiko in popFourmis:
            self.delete(self.fourmis.pop(Himiko))

        for Maki in pushFourmilieres:
            self.dessinFourmiliere(Maki)
        for Aoi in popFourmilieres:
            self.delete(self.nests.pop(Aoi))

        for Mikan in pushPheros:
            self.dessinPheromone(Mikan, pheromones[Mikan][0].typeP)
        for Chihiro in popPheros:
            self.delete(self.pheros.pop(Chihiro))

        for Mahiru in pushRessources:
            self.dessinRessource(Mahiru)
        for Peko in popRessources:
            self.delete(self.ress.pop(Peko))

        for Kaito in pushObstacles:
            self.dessinObstacle(Kaito)
        for Shuichi in popObstacles:
            self.delete(self.obstacles.pop(Shuichi))

        for Toko in self.ress: # mise à jour de la couleur des ressources
            self.itemconfig(self.ress[Toko], fill = self.couleurRess(ressources[Toko], tailleMax))

        for x in self.pheros:
            self.itemconfig(self.pheros[x], fill = self.couleurPhero(pheromones[x][-1].duree, pheromones[x][-1].typeP))
