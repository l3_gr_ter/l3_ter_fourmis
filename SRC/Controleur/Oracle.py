from collections import deque, defaultdict
from random import Random

from Outils.Coordonnees import Coordonnees
from Outils.Direction import Direction
from Modele.Fourmiliere import Fourmiliere
from Modele.Fourmi import Fourmi
from Modele.Pheromone import Pheromone
from Modele.TypePheromone import TypePheromone


class Oracle:
    """
    Fais battre l'horloge de tous les éléments de la simulation
    Attributs :
    - fourmilieres, liste de Fourmiliere
    - fourmis, liste de Fourmi
    - pheromones, liste de Pheromone
    - sources, liste de Source

    Méthode :
    - tick(), déroulement d'un coup d'horloge pour les éléments de la simulation
    """

    # obstacles entourant le périmètre de la grille
    obstaclesDefaut = set()

    def __init__(self, graine):
        # récupération de la graine
        self.graine = graine
        self.randGen = Random(self.graine)

        self.fourmilieres = {}               # {Coordonnees: Fourmiliere}
        self.fourmis = defaultdict(list)     # {Coordonnees: [Fourmi...]}
        self.pheromones = defaultdict(deque) # {Coordonnees: deque(Pheromone...)}
        self.ressources = {}                 # {Coordonnees: taille de la ressource}
        self.obstacles = set()               # {Coordonnees} (coordonnées des obstacles)



    def __repr__(self):
        return f"""
Oracle[
    Fourmilières{self.fourmilieres}

    Fourmis{repr(self.fourmis)[28:]}

    Phéromones{repr(self.pheromones)[41:]}

    Ressources{self.ressources}

    Obstacles{self.obstacles}
]"""


    def setup(self, coordFrmliere, nbFourmis, tailleMaxRess, coordRess):
        "Initialise le contrôleur, en lui donnant le centre supérieur de la grille pour y placer la fourmilière, et le centre inférieur pour y placer la ressource"
        idF = self.ajoutFourmiliere(coordFrmliere)
        self.ajoutFourmis(coordFrmliere, idF, nbFourmis)
        self.tailleMaxRess = tailleMaxRess
        self.ajoutRessource(coordRess, self.tailleMaxRess)
        self.obstacles = Oracle.obstaclesDefaut


    def ajoutFourmiliere(self, coord):
        "Ajoute ou écrase une fourmilière aux coordonnées et retourne son identifiant"
        self.fourmilieres[coord] = Fourmiliere()
        return self.fourmilieres[coord].id


    def ajoutFourmis(self, coord, id, n=1):
        "Ajoute n fourmis aux coordonnées avec l'identifiant de sa fourmilière"
        fourmis = [Fourmi(id) for i in range(0, n)]
        self.fourmis[coord].extend(fourmis)


    def ajoutRessource(self, coord, taille):
        "Ajoute ou écrase une ressource aux coordonnées en donnant sa taille"
        self.ressources[coord] = taille

    def supprimeFourmiliere(self, coord):
        "Supprime une fourmilière aux coordonnées coord et toutes les fourmis lui étant associées, fonction utilisable seulement lors de la phase d'initialisation"
        popfourmis = set() #sets de fourmi vides à pop
        for fourmis in self.fourmis:
            if fourmis == coord:
                popfourmis.add(fourmis)
        for suppr in popfourmis:
            self.fourmis.pop(suppr)
        self.fourmilieres.pop(coord)

    def supprimeRessource(self, coord):
        "Supprime une ressource aux coordonnées coord"
        self.ressources.pop(coord)

    def supprimeObstacle(self, coord):
        "Supprime un obstacle aux coordonnées coord"
        self.obstacles.remove(coord)


    def avanceTicks(self, n):
        "Avance l'oracle de n ticks"
        while n > 0:
            self.tick()
            n -= 1


    def pheromonesAlentour(self, coord, directions):
        "Retourne la liste des listes de phéromones dans les directions autour de la coord en paramètres"
        return [([pheromone for pheromone in self.pheromones[coord + direction.value]] if coord + direction.value in self.pheromones else []) for direction in directions]

        """ équivalent à
        res = []
        for dir in directions:
            sub = []
            if coord + dir.value in self.pheromones:
                for pheromone in self.pheromones[coord + dir]:
                    sub.append(pheromone)
            res.append(sub)
        return res
        """


    def tick(self):
        "Exécute le tick de chaque attribut"
        nouvFourmis = defaultdict(list)
        nouvPheromones = defaultdict(deque)

        # ajout d'une phéromone de type Retour sur chaque fourmilière
        for coord, fourmiliere in self.fourmilieres.items():
            pheromone = fourmiliere.tick()
            nouvPheromones[coord].append(pheromone)

        # tick des fourmis
        for coord, listeFourmis in self.fourmis.items():
            # liste des directions sans obstacle
            directions = [direction for direction in Direction if coord + direction.value not in self.obstacles]

            # liste de liste des types de phéromones dans chaque direction atteignable
            pheromonesAlentour = self.pheromonesAlentour(coord, directions)

            # la ressource sur ces coord si elle existe, faux sinon
            ressource = (self.ressources[coord] if coord in self.ressources else False)

            # si les coordonnées sont partagées avec une fourmilière
            fourmiliere = self.fourmilieres.get(coord, False)

            # exécution du tick pour chaque fourmi à ces coordonnées
            for fourmi in listeFourmis:
                typePheromone = fourmi.tick(self.randGen, directions, pheromonesAlentour, ressource, fourmiliere) # retourne le TypePheromone à déposer
                pheromone = Pheromone(fourmi.idFourmiliere, typePheromone)

                # récolte de la ressource si présente
                if ressource is not False and ressource > 0:
                    ressource -= 1

                # ajout de la phéromone
                nouvPheromones[coord].append(pheromone)

                # les coordonnées de la fourmi sont modifiées si elle a une direction
                nouvCoord = (coord + fourmi.direction.value) if fourmi.direction else coord

                # ajout de la fourmi
                nouvFourmis[nouvCoord].append(fourmi)

            # mise à jour du dictionnaire des ressources

            if ressource is not False:
                if ressource == 0:
                    self.ressources.pop(coord)
                else:
                    self.ressources[coord] = ressource



        # mise à jour du dictionnaire des fourmis par réaffectation
        self.fourmis = nouvFourmis

        # tick des phéromones
        for coord, listePheromones in self.pheromones.items():
            # tick puis suppression des phéromones évaporées
            while listePheromones:
                listePheromones[0].tick()
                if listePheromones[0].epuisee():
                    listePheromones.popleft()
                else:
                    break

            # tick des phéromones restantes
            for pheromone in list(listePheromones)[1:]:
                pheromone.tick()

        # mise à jour du dictionnaire des phéromones par ajout
        for coord in nouvPheromones:
            self.pheromones[coord].extend(nouvPheromones[coord])

        # suppression des clés inutilisées
        for cle in self.pheromones.copy():
            if not self.pheromones[cle]:
                del self.pheromones[cle]
