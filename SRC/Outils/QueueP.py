from heapq import heappop, heappush


class QueueP:
    "Une queue prioritaire utilisant les fonctions du module heapq"

    def __init__(self):
        self.elements = []
        self.id = 0 # identifiant unique à chaque élément inséré, permet de trier des priorités égales


    def push(self, priorite, element):
        "Récupère priorité et élément et insère (priorité, id, élément) dans le tas"
        heappush(self.elements, (priorite, self.id, element))
        self.id += 1


    def pop(self):
        "Retourne l'élément du tas avec la priorité la plus élevée"
        return heappop(self.elements)[2]


    def __contains__(self, element):
        "Retourne vrai si element est dans la queue"
        for _, _, elem in self.elements:
            if element == elem:
                return True
        return False


    def __bool__(self):
        "Retourne vrai si la queue n'est pas vide"
        return bool(self.elements)
