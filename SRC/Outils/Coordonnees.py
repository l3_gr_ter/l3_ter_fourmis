from Outils.Direction import Direction


class Coordonnees:
    """
    Représente des coordonnées (x, y) constantes dans un plan cartésien
    """
    
    def __init__(self, x = 0, y = 0):
        "Prend x et y, par défaut (0, 0)"
        self.x = x
        self.y = y


    def __repr__(self):
        return "(x:" + str(self.x) + ",y:" + str(self.y) + ")"

    
    def __eq__(self, coord):
        return self.x == coord.x and self.y == coord.y


    def __add__(self, tup):
        "Retourne ces Coordonnees additionnées à un tuple (a, b)"
        return Coordonnees(self.x + tup[0], self.y + tup[1])


    def __sub__(self, tup):
        "Retourne ces Coordonnees soustraites à un tuple (a, b)"
        return Coordonnees(self.x - tup[0], self.y - tup[1])


    def __hash__(self):
        "Utilisé par set() et dict()"
        return hash((self.x, self.y))


    def __getitem__(self, i):
        "Retourne la valeur à l'index : 0 -> x, 1 -> y"
        if i == 0:
            return self.x
        if i == 1:
            return self.y
        raise IndexError


    def distance(self, coord, norme=0):
        "Distance entre ces Coordonnees et un tuple (x, y) dans une norme (0 par défaut)"
        if norme == 1:
            return abs(self.x - coord[0]) + abs(self.y - coord[1]) # norme 1, distance Manhattan, voisinage de Von Neumann
        if norme == 2:
            return (((self.x - coord[0]) ** 2) + ((self.y - coord[1]) ** 2)) ** 0.5 # norme 2, distance Euclidienne
        return max(abs(self.x - coord[0]), abs(self.y - coord[1])) # norme 0, distance sup, voisinage de Moore


    def voisinage(self, interdit=[]):
        "Retourne la liste des voisins qui ne sont pas interdits"
        return [self + direction for direction in Direction.valeurs() if self + direction not in interdit]

    
    def direction(self, coord):
        "Retourne la direction de ces Coordonnees à un tuple (x, y) en paramètre"
        a = coord[0] - self.x
        if a != 0:
            a //= abs(a)
        b = coord[1] - self.y
        if b != 0:
            b //= abs(b)
        return (a, b)
