from Outils.Direction import Direction
from Outils.QueueP import QueueP


def consChemin(vientDe, coord):
    "Reconstruit le chemin depuis coord"
    chemin = [coord]
    while coord in vientDe:
        coord = vientDe[coord]
        chemin.append(coord)
    return chemin



def AStar(depart, arrivee, obstacles, norme=0):
    "Une implémentation de l'algorithme A*"
    
    aVisiter = QueueP()      # queue prioritaire de Coordonnees des cases à visiter
    vientDe = {}             # coord => coord desquelles on est venu sur la clé
    distDepart = {depart: 0} # coord => distance connue du départ

    aVisiter.push(0, depart) # le départ est la première case à visiter

    while aVisiter: # tant qu'il reste des cases à visiter
        coord = aVisiter.pop()

        # si l'arrivée est atteinte
        if coord == arrivee:
            return consChemin(vientDe, coord)
        
        # pour chaque voisin sans obstacle
        for voisin in coord.voisinage(obstacles):
            # calcul de la distance connue du départ au voisin en passant par la case actuelle
            distance = distDepart[coord] + 1

            # découverte d'un nouveau chemin plus court
            if voisin not in distDepart or distance < distDepart[voisin]:
                vientDe[voisin] = coord
                distDepart[voisin] = distance

                # ajout d'une nouvelle case à visiter
                if voisin not in aVisiter:
                    # distance théorique du voisin à l'arrivée, plus elle est petite et plus sa priorité de visite est élevée
                    distArrivee = distDepart[coord] + voisin.distance(arrivee, norme)
                    aVisiter.push(distArrivee, voisin)

    # s'il n'y a plus aucune case à visiter sans avoir trouvé l'arrivée
    #print("Aucun chemin trouvé de", depart, "à", arrivee)





class AStarObj:
    "Une classe implémentant A*, qui exécute une étape à chaque appel de tick()"

    def __init__(self, depart, arrivee, obstacles, norme=0):
        # conservation des paramètres
        self.depart = depart
        self.arrivee = arrivee
        self.obstacles = obstacles
        self.norme = norme

        self.chemin = []  # le chemin trouvé du départ à l'arrivée
        self.coord = None # les Coordonnees actuellement étudiées
        self.nbrTicks = 0 # nombre de ticks écoulés depuis le début de l'exécution

        self.aVisiter = QueueP()      # queue prioritaire de Coordonnees des cases à visiter
        self.vientDe = {}             # coord => coord desquelles on est venu sur la clé
        self.distDepart = {depart: 0} # coord => distance connue du départ

        self.aVisiter.push(0, depart) # le départ est la première case à visiter


    def tick(self):
        "Exécute une étape de l'algorithme"
        if self.chemin: # arrêt de l'exécution si une solution est déjà trouvée
            return

        self.nbrTicks += 1
            
        if self.aVisiter: # tant qu'il reste des cases à visiter
            self.coord = self.aVisiter.pop()

            # si l'arrivée est atteinte
            if self.coord == self.arrivee:
                self.chemin = consChemin(self.vientDe, self.coord)
                print("Chemin", self.depart, "-", self.arrivee, "de longueur", len(self.chemin), "trouvé en", self.nbrTicks, "ticks")
                return True
            
            # pour chaque voisin sans obstacle
            for voisin in [self.coord + direction.value for direction in Direction if self.coord + direction.value not in self.obstacles]:
                # calcul de la distance connue du départ au voisin en passant par la case actuelle
                distance = self.distDepart[self.coord] + 1

                # découverte d'un nouveau chemin plus court
                if voisin not in self.distDepart or distance < self.distDepart[voisin]:
                    self.vientDe[voisin] = self.coord
                    self.distDepart[voisin] = distance

                    # ajout d'une nouvelle case à visiter
                    if voisin not in self.aVisiter:
                        # distance théorique du voisin à l'arrivée, plus elle est petite et plus sa priorité de visite est élevée
                        distArrivee = self.distDepart[self.coord] + voisin.distance(self.arrivee, self.norme)
                        self.aVisiter.push(distArrivee, voisin)

        # s'il n'y a plus aucune case à visiter sans avoir trouvé l'arrivée
        else:
            self.chemin.append(None)
            print("Aucun chemin trouvé de", self.depart, "à", self.arrivee)
