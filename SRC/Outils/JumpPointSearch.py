from Outils.Direction import Direction
from Outils.Coordonnees import Coordonnees
from Outils.QueueP import QueueP



def consChemin(vientDe, depart, arrivee):
    "Reconstruit le chemin de depart à arrivee"
    # seuls les points de saut et leur direction sont enregistrés
    chemin = [arrivee]
    coord = arrivee
    while coord in vientDe:
        parent, direction = vientDe[coord]
        while True: # ligne d'un point de saut à un autre
            coord = coord - direction
            chemin.append(coord)
            if coord == parent:
                break
    return chemin



def directionsPossibles(coord, direction, obstacles):
    """
    Retourne les directions pour rejoindre le voisinage de coord
    ce voisinage est soustrait aux obstacles
    et élagué selon les règles de l'algorithme Jump Point Search
    """
    directions = [] # liste de tuples (Coordonnees, direction)

    # les voisins forcés sont insérés avant les voisins naturels
    # car ils sont plus prometteurs d'atteindre l'arrivée

    # déplacement vertical Nord/Sud
    if direction[0] == 0:
        # obstacle à l'Est et la case qui lui est adjacente dans la direction n'en est pas un
        if coord + (1, 0) in obstacles and coord + (1, direction[1]) not in obstacles:
            directions.append((1, direction[1])) # voisin forcé

        # pareil pour obstacle à l'Ouest
        if coord + (-1, 0) in obstacles and coord + (-1, direction[1]) not in obstacles:
            directions.append((-1, direction[1])) # voisin forcé

    # déplacement horizontal Est/Ouest
    elif direction[1] == 0:
        # obstacle au Nord et la case qui lui est adjacente dans la direction n'en est pas un
        if coord + (0, -1) in obstacles and coord + (direction[0], -1) not in obstacles:
            directions.append((direction[0], -1)) # voisin forcé

        # pareil pour obstacle au Sud
        if coord + (0, 1) in obstacles and coord + (direction[0], 1) not in obstacles:
            directions.append((direction[0], 1)) # voisin forcé

    # déplacement diagonal
    else:
        # obstacle à l'Est/Ouest et pas à l'opposé sur l'axe Y
        if coord + (-direction[0], 0) in obstacles and coord + (-direction[0], direction[1]) not in obstacles:
            directions.append((-direction[0], direction[1])) # voisin forcé

        # obstacle au Nord/Sud et pas à l'opposé sur l'axe X
        if coord + (0, -direction[1]) in obstacles and coord + (direction[0], -direction[1]) not in obstacles:
            directions.append((direction[0], -direction[1])) # voisin forcé
        
        if coord + (direction[0], 0) not in obstacles: # pas d'obstacle dans l'axe X
            directions.append((direction[0], 0)) # voisin naturel

        if coord + (0, direction[1]) not in obstacles: # pas d'obstacle dans l'axe Y
            directions.append((0, direction[1])) # voisin naturel

    # pas d'obstacle dans la direction
    if coord + direction not in obstacles:
        directions.append(direction) # naturel
    
    return directions
# fin directionsPossibles()



def voisinForce(coord, direction, obstacles):
    "Retourne vrai si un voisin de coord est forcé selon les règles de directionsPossibles()"
    if direction[0] == 0: # déplacement vertical Nord/Sud
        if (coord + (1, 0) in obstacles and coord + (1, direction[1]) not in obstacles
        or coord + (-1, 0) in obstacles and coord + (-1, direction[1]) not in obstacles):
            return True

    elif direction[1] == 0: # déplacement horizontal Est/Ouest
        if (coord + (0, -1) in obstacles and coord + (direction[0], -1) not in obstacles
        or coord + (0, 1) in obstacles and coord + (direction[0], 1) not in obstacles):
            return True

    else: # déplacement en diagonal
        if (coord + (-direction[0], 0) in obstacles and coord + (-direction[0], direction[1]) not in obstacles # obstacle Est/Ouest
        or coord + (0, -direction[1]) in obstacles and coord + (direction[0], -direction[1]) not in obstacles): # obstacle Nord/Sud
            return True

    return False # aucun voisin forcé



def identifieSuccesseurs(coord, direction, arrivee, obstacles, distanceMax):
    """
    Exécute un saut dans chaque direction possible
    Retourne la liste des points prometteurs
    """
    successeurs = [] # liste des successeurs

    if direction is None: # première recherche de successeurs
        directions = [direction for direction in Direction.valeurs() if coord + direction not in obstacles]
    else: # calcul des directions en appliquant des règles
        directions = directionsPossibles(coord, direction, obstacles)

    for direction in directions:
        # exploration dans la direction
        point = saut(coord, direction, arrivee, obstacles, distanceMax)

        if point is not None: # ajout d'un point d'intérêt trouvé
            if point == arrivee: # mise en avant de l'arrivée
                return [point]

            successeurs.append(point)

    return successeurs



def saut(coord, direction, arrivee, obstacles, distanceMax):
    """
    Avance dans la direction récursivement
    Retourne coord si un point d'intérêt est trouvé
    None sinon
    """
    coord = coord + direction

    if coord in obstacles: # obstacle
        return None

    # sur l'arrivée
    if coord == arrivee:
        return coord

    if voisinForce(coord, direction, obstacles): # au moins un voisin forcé
        return coord

    # arrêt de la récursion quand on s'éloigne trop de l'arrivée
    if arrivee.distance(coord) >= distanceMax and arrivee.distance(coord) < arrivee.distance(coord + direction):
        return coord

    # si direction diagonale
    if direction[0] != 0 and direction[1] != 0:
        # rencontre d'un axe de l'arrivée
        if coord.x == arrivee.x or coord.y == arrivee.y:
            return coord

        # récursion sur les deux axes
        
        if (saut(coord, (direction[0], 0), arrivee, obstacles, distanceMax) is not None
        or saut(coord, (0, direction[1]), arrivee, obstacles, distanceMax) is not None):
            return coord

    return saut(coord, direction, arrivee, obstacles, distanceMax) # appel récursif





def JPS(depart, arrivee, obstacles):
    "Une implémentation de l'algorithme Jump Point Search"

    aVisiter = QueueP()      # queue prioritaire de Coordonnees des cases à visiter
    vientDe = {}             # coord => (coord, direction) desquels on est venu sur la clé
    distDepart = {depart: 0} # coord => distance connue du départ

    aVisiter.push(0, depart) # le départ est la première case à visiter

    # un saut n'est pas autorisé à aller plus loin d'une case si 
    # sa distance à l'arrivée est >= à la distance du départ à l'arrivée
    distanceMax = depart.distance(arrivee)

    while aVisiter: # tant qu'il reste des cases à visiter
        coord = aVisiter.pop()

        # l'arrivée est trouvée
        if coord == arrivee:
            return consChemin(vientDe, depart, arrivee)

        # récupération de la direction depuis laquelle on arrive sur coord
        direction = vientDe[coord][1] if coord in vientDe else None

        # calcul des successeurs
        successeurs = identifieSuccesseurs(coord, direction, arrivee, obstacles, distanceMax)

        for point in successeurs:
            # calcul de la distance du départ au successeur
            distance = distDepart[coord] + coord.distance(point)

            # ignoré si déjà visité avec un meilleur chemin
            if point not in distDepart or distance < distDepart[point]:
                # découverte d'un nouveau chemin plus court
                vientDe[point] = (coord, coord.direction(point))
                distDepart[point] = distance

                # ajout d'une nouvelle case à visiter
                if point not in aVisiter:
                    distArrivee = distance + point.distance(arrivee)
                    aVisiter.push(distArrivee, point)

    # s'il n'y a plus aucune case à visiter sans avoir trouvé l'arrivée
    print("JPS: Aucun chemin trouvé de", depart, "à", arrivee)
# fin de JPS



class JPSObj:
    "Une classe implémentant JumpPointSearch, qui exécute une étape à chaque appel de tick()"

    def __init__(self, depart, arrivee, obstacles):
        # conservation des paramètres
        self.depart = depart
        self.arrivee = arrivee
        self.obstacles = obstacles

        self.chemin = []  # le chemin trouvé du départ à l'arrivée
        self.coord = None # les Coordonnees actuellement étudiées
        self.nbrTicks = 0 # nombre de ticks écoulés depuis le début de l'exécution

        self.aVisiter = QueueP()      # queue prioritaire de Coordonnees des cases à visiter
        self.vientDe = {}             # coord => coord desquelles on est venu sur la clé
        self.distDepart = {depart: 0} # coord => distance connue du départ

        self.aVisiter.push(0, depart) # le départ est la première case à visiter

        # un saut doit s'arrêter à faire plus d'une itération si :
        #  distance de la case courante à l'arrivée >= distance du départ à l'arrivée
        #  et la prochaine case dans la direction du saut s'éloigne de l'arrivée
        self.distanceMax = depart.distance(arrivee)


    def tickJPS(self):
        # récupération de la direction depuis laquelle on arrive sur coord
        direction = self.vientDe[self.coord][1] if self.coord in self.vientDe else None

        # calcul des successeurs
        successeurs = identifieSuccesseurs(self.coord, direction, self.arrivee, self.obstacles, self.distanceMax)

        for point in successeurs:
            # calcul de la distance du départ au successeur
            distance = self.distDepart[self.coord] + self.coord.distance(point)

            # ignoré si déjà visité avec un meilleur chemin
            if point not in self.distDepart or distance < self.distDepart[point]:
                # découverte d'un nouveau chemin plus court
                self.vientDe[point] = (self.coord, self.coord.direction(point))
                self.distDepart[point] = distance

                # ajout d'une nouvelle case à visiter
                if point not in self.aVisiter:
                    distArrivee = distance + point.distance(self.arrivee)
                    self.aVisiter.push(distArrivee, point)


    def tick(self):
        "Exécute une étape de l'algorithme"
        if self.chemin:
            return
        
        if self.aVisiter: # tant qu'il reste des cases à visiter
            self.coord = self.aVisiter.pop()
            self.nbrTicks += 1

            # l'arrivée est trouvée
            if self.coord == self.arrivee:
                self.chemin = consChemin(self.vientDe, self.depart, self.arrivee)
                #print("JPSObj: Chemin", self.depart, "-", self.arrivee, "de longueur", len(self.chemin), "trouvé en", self.nbrTicks, "ticks")
                return True

            self.tickJPS()

        else: # s'il n'y a plus aucune case à visiter sans avoir trouvé l'arrivée
            self.chemin.append(None)
            #print("JPSObj: Aucun chemin trouvé de", self.depart, "à", self.arrivee)
            return False
# fin de JPSObj





def JPSBidir(depart, arrivee, obstacles):
    "Une implémentation bidirectionnelle de l'algorithme Jump Point Search"

    # une instance de JPS sur le départ et l'arrivée
    aller = JPSObj(depart, arrivee, obstacles)
    retour = JPSObj(arrivee, depart, obstacles)

    while True:
        if aller.nbrTicks == retour.nbrTicks:
            # tick de aller
            res = aller.tick()

            if res is None: # la recherche continue
                continue

            if res is False: # aucun chemin possible
                return False

            # res est vrai, chemin trouvé
            chemin = aller.chemin
            break
            
        else: # tick de retour et corps de l'algorithme
            res = retour.tick()

            if res is False: # aucun chemin possible
                return False

            elif res is True: # chemin trouvé
                chemin = retour.chemin
                break

            # intersection des cases visitées par les deux têtes
            intersection = aller.vientDe.keys() & retour.vientDe.keys()

            if intersection: # il existe une case visitée commune entre aller et retour
                # récupération de la case en question
                pivot = intersection.pop()

                # construction du chemin
                cheminAller = consChemin(aller.vientDe, depart, pivot)
                cheminRetour = consChemin(retour.vientDe, arrivee, pivot)
                chemin = cheminRetour[::-1] + cheminAller[1:]
                break

    return chemin
# fin de JPSBidir



class JPSBidirObj:
    "Une implémentation objet bidirectionnelle de l'algorithme Jump Point Search"

    def __init__(self, depart, arrivee, obstacles):
        # conservation des paramètres
        self.depart = depart
        self.arrivee = arrivee
        self.obstacles = obstacles

        # une instance de JPS sur le départ et l'arrivée
        self.aller = JPSObj(depart, arrivee, obstacles)
        self.retour = JPSObj(arrivee, depart, obstacles)

        self.chemin = []    # le chemin trouvé du départ à l'arrivée
        self.nbrTicks = 0   # nombre de ticks écoulés depuis le début de l'exécution


    def tick(self):
        if self.chemin:
            return

        self.nbrTicks += 1

        if self.nbrTicks % 2:
            # tick de aller
            res = self.aller.tick()

            if res is None: # la recherche continue
                return

            if res is False: # aucun chemin possible
                self.chemin.append(None)
                return False

            # chemin trouvé
            self.chemin = self.aller.chemin
            return True
            
        else: # tick de retour
            res = self.retour.tick()

            if res is False: # aucun chemin possible
                self.chemin.append(None)
                return False

            elif res is True: # chemin trouvé
                self.chemin = self.retour.chemin
                return True

            # intersection des cases visitées par les deux têtes
            intersection = self.aller.vientDe.keys() & self.retour.vientDe.keys()

            if intersection: # case commune entre aller et retour
                pivot = intersection.pop()
                cheminAller = consChemin(self.aller.vientDe, self.depart, pivot)
                cheminRetour = consChemin(self.retour.vientDe, self.arrivee, pivot)
                self.chemin = cheminRetour[::-1] + cheminAller[1:]

                print("BidirObj: Chemin", self.depart, "-", self.arrivee, "de longueur", len(self.chemin), "trouvé en", self.nbrTicks, "ticks")
                return True
