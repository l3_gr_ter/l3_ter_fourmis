from enum import Enum

class Direction(Enum):
    """
    Défini une direction valuée à des coordonnées relatives, parmi :
    N, E, S, O, NE, SE, SO, NO
    """
    
    N =   0,-1
    E =   1, 0
    S =   0, 1
    O =  -1, 0
    #"""
    NE =  1,-1
    SE =  1, 1
    SO = -1, 1
    NO = -1,-1
    #"""
    

    def inverse(self):
        "Retourne la Direction inverse"
        inv = self.value[0] * -1, self.value[1] * -1
        return Direction(inv) # accès par valeur

    def valeurs():
        "Retourne la liste des valeurs des directions"
        return [direction.value for direction in Direction]
