from random import choice, choices

from Modele.TypeFourmi import TypeFourmi
from Modele.TypePheromone import TypePheromone

class Fourmi:
    """
    Une fourmi possède les attributs :
    - idFourmiliere: identifiant unique de sa fourmilière,
    - typeF: TypeFourmi,
    - depose: TypePheromone, que la fourmi dépose,
    - cherche: TypePheromone, que la fourmi cherche à suivre,
    - direction: Direction, dans laquelle la fourmi se dirige
    """

    def __init__(self,
                 idFourmiliere,
                 typeF = TypeFourmi.Ouvriere,
                 depose = TypePheromone.Retour,
                 cherche = TypePheromone.Aller):
        self.idFourmiliere = idFourmiliere
        self.typeF = typeF
        self.depose = depose
        self.cherche = cherche
        self.direction = None # pas de direction


    def __repr__(self):
        return f"typeF: {self.typeF}, depose: {self.depose}, cherche: {self.cherche}, direction: {self.direction}, idFourmiliere: {self.idFourmiliere}"


    def reconnait(self, pheromone):
        "Retourne vrai si la fourmi reconnait la phéromone comme provenant de sa fourmilière"
        return self.idFourmiliere == pheromone.idFourmiliere


    def tick(self, randGen, directions, pheromonesAlentour, ressource, fourmiliere):
        "Fais un choix de direction en fonction des phéromones qui s'y trouvent et retourne le TypePheromone à déposer"
        # filtre des phéromones alentour ne gardant que les phéromones reconnues
        pheromonesAlentour = [[pheromone.typeP for pheromone in pheromones if self.reconnait(pheromone)] for pheromones in pheromonesAlentour]

        # dépôt de ressource dans la fourmilière
        if fourmiliere and self.cherche == TypePheromone.Retour:
            fourmiliere.depotRessource()
            self.depose, self.cherche = self.cherche, self.depose

        # si la fourmi cherche et trouve une ressource
        if ressource and self.cherche == TypePheromone.Aller:
            self.depose, self.cherche = self.cherche, self.depose

        # aucune direction possible
        if not directions:
            self.direction = None
            return self.depose

        # liste du nombre de phéromones recherchées par direction
        nbCherche = [1 + pheromones.count(self.cherche) for pheromones in pheromonesAlentour]

        # on décide de laisser le choix dans la classe Fourmi pour une raison de sémantique
        if sum(nbCherche) > len(nbCherche): # si au moins une phéromone recherchée dans le voisinage
            self.direction = randGen.choices(directions, nbCherche)[0]
        else: # sinon mouvement Brownien
            self.direction = randGen.choice(directions)

        return self.depose
