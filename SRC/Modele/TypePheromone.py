from enum import Enum

class TypePheromone(Enum):
    """
    Le type d'une phéromone parmi :
    Aller, Retour, Fuir, Attaquer
    """

    Aller = 0
    Retour = 1
    Fuir = 2
    Attaquer = 3
