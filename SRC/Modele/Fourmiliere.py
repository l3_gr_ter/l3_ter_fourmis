from Modele.Pheromone import Pheromone
from Modele.TypePheromone import TypePheromone


class Fourmiliere:
    """
    Une fourmilière possède les attributs :
    - id: identifiant unique
    """

    # identifiant de la prochain instance de Fourmiliere
    nouvId = 0

    def __init__(self):
        self.id = Fourmiliere.nouvId
        Fourmiliere.nouvId += 1 # incrémentation de l'attribut de classe pour garder l'identifiant unique
        self.ressourcesDeposees = 0


    def __repr__(self):
        return f"id: {self.id}, ressourcesDeposees: {self.ressourcesDeposees}"


    def depotRessource(self):
        self.ressourcesDeposees += 1


    def tick(self):
        "Retourne une phéromone Retour à déposer"
        return Pheromone(self.id, TypePheromone.Retour)