from enum import Enum

class TypeFourmi(Enum):
    """
    Le type d'une fourmi parmi :
    Ouvriere, Soldat
    """

    Ouvriere = 0
    Soldat = 1
