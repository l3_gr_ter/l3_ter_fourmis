from Modele.TypePheromone import TypePheromone

class Pheromone:
    """
    Une Pheromone possède les attributs :
    - idFourmiliere, identifiant unique de la fourmilière de la fourmi ayant déposé la phéromone
    - type, PheromoneType
    - duree, la durée de vie restante de la phéromone
    """

    def __init__(self, idFourmiliere, typeP):
        "Prend un identifiant de fourmilière et un TypePheromone en paramètres"
        self.idFourmiliere = idFourmiliere
        self.typeP = typeP
        self.duree = Pheromone.duree


    def __repr__(self):
        return f"idFourmiliere: {self.idFourmiliere}, Pheromone[typeP: {self.typeP.name}, duree: {self.duree}]"


    def epuisee(self):
        "Retourne vrai si la phéromone est épuisée"
        return self.duree <= 0


    def tick(self):
        "Décrémente la durée"
        self.duree -= 1
