from tkinter import Tk, PhotoImage

from Vue.Interface import Interface


# la racine de l'environnement Tk
racine = Tk()

# icone et titre du programme
racine.tk.call('wm', 'iconphoto', racine._w, PhotoImage(file = './icon.png'))
racine.wm_title('ANTS : Simulation')

# création de la simulation
interface = Interface(racine)


def main():
    tickSpeed = interface.tick() # récupération du temps en ms avant la prochaine exécution
    racine.after(tickSpeed, main) # rééxécuter dans tickSpeed ms


# réception d'événement
racine.after(0, main) # exécuter main() dès le lancement de mainloop
racine.mainloop()
